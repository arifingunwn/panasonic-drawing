var express = require('express');
var mustacheExpress = require('mustache-express');
var handlebars = require('express-handlebars');
var app = express();
var bodyParser = require('body-parser');
var http = require('http').Server(app);
var io = require('socket.io')(http,{
    pingInterval: 1000
});

var __ = require('underscore');

//app.engine('html', mustacheExpress());
//app.set('view engine', 'mustache');
app.engine('html', handlebars());
app.set('view engine', 'handlebars');

app.set('views', __dirname + '\\src');
app.set('view cache', false);
app.use("/src", express.static(__dirname + '\\src'));

var jsonParser = bodyParser.json();

var model = require('./models/main');

app.get('/drawing/:category/:prizeCode', function(req, res){
  //res.sendFile(__dirname + '\\src\\index.html');
  model.getDrawingParameter(req.params.category, req.params.prizeCode, function(data){
    model.getDrawingPool(req.params.category, function(pool){
        model.getWinDrawingNumber(req.params.category, req.params.prizeCode, function(winNumber){
            console.log(winNumber);
            res.render('index.html', { 
                categoryName: data[0].CategoryName, 
                categoryCode: data[0].CategoryCode, 
                priceName: data[0].PrizeName,  
                pool: pool,
                prizeQuota: data[0].PrizeQuota,
                prizeCode: data[0].PrizeCode,
                winner: winNumber
            });
        });
    });  
  });
});

app.post('/InsertDrawingWinner', jsonParser, function (req, res) {
    var data = {
        winNumber: req.body.winNumber,
        catCode: req.body.catCode,
        prizeCode: req.body.prizeCode
    };

    model.insertWinNumber(data, function(){
        res.setHeader('Content-Type', 'application/json');
        res.end('{"success" : "Updated Successfully", "status" : 200}');
        console.log('Success insert drawing number.');
    })
});

app.post('/GetRandomNumber', jsonParser, function(req, res){
    var catCode = req.body.catCode;

    model.getRandomCode(catCode, function(e){
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify({ randomNumber: e.randomNumber }));
    });
});

app.post('/GetPrizeByCategory', jsonParser, function(req, res){
    var catCode = req.body.catCode;

    model.getPrizeByCategoryCode(catCode, function(result){
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify({ PrizeCode: result }));
    });
});

app.get('/button-stop-1', function(req, res){
    res.sendFile(__dirname + '\\src\\button-stop-1.html');
});

app.get('/button-stop-2', function(req, res){
    res.sendFile(__dirname + '\\src\\button-stop-2.html');
});

app.get('/admin-panel', function(req, res){
    res.sendFile(__dirname + '\\src\\admin-panel.html');
});

app.get('/result', function(req, res){
    res.sendFile(__dirname + '\\src\\index-result.html');
});

app.get('/result/:catCode/:prizeCode', function(req, res){
    var categoryCode = req.params.catCode.toUpperCase();
    var prizeCode = req.params.prizeCode.toString().toUpperCase().split(';');
   
    model.getDrawingResultByPrizeCode(categoryCode, prizeCode, function(result){
            res.render('result.html', {
                prizeCode: prizeCode,
                categoryCode: categoryCode,
                categoryName: result.length > 0 ? result[0].categoryName : '',
                data: __.groupBy(result, 'PrizeCode'),
                helpers: {
                    removeDot: function(text){
                        return text.toString().replace('.', '');
                    },
                    getColor: function(cat){
                        var color = '';
                        if(cat.toUpperCase() == 'BSOG'){
                            color = '#e9b03d';
                        } else if (cat.toUpperCase() == 'BSOP') {
                            color = '#878787';
                        } else if (cat.toUpperCase() == 'ACAC') {
                            color = '#009ce4';
                        } else {
                            color = '#29a277';
                        }

                        return color;
                    }
                }
            });
    });
});

app.get('/result-bsog', function(req, res){
    model.getDrawingResult('BSOG', function(result){
        res.render('result-gold.html', __.groupBy(result, 'PrizeCode'));
    });
});

app.get('/result-bsop', function(req, res){
    model.getDrawingResult('BSOP', function(result){
        res.render('result-platinum.html', __.groupBy(result, 'PrizeCode'));
    });
});

app.get('/result-ac', function(req, res){
    model.getDrawingResult('ACAC', function(result){
        res.render('result-ac.html', __.groupBy(result, 'PrizeCode'));
    });
});

app.get('/result-fridge', function(req, res){
    model.getDrawingResult('FRDG', function(result){
        res.render('result-fridge.html', __.groupBy(result, 'PrizeCode'));
    });
});

app.get('/report', function(req, res){
    model.getReportWinner(function(data){
        res.render('report-winner.html', { data: data });
    });
});

app.get('/report/UnclaimedWinNumber/:catCode/:prizeCode', function(req, res){
    model.getUnclaimedWinNumber(req.params.catCode, req.params.prizeCode, function(data){
        res.render('report-unclaimed.html', { data: data });
    });
});

app.get('/utility', function(req, res){
   res.render('utility.html');   
});

app.post('/utility/void', jsonParser, function(req, res){
    var nomorUndian = req.body.NomorUndian;
    model.voidNomorUndian(nomorUndian, function(result){
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify({ result }));
    });
});

app.post('/utility/generate', jsonParser, function(req, res){
    var data = {
        categoryCode: req.body.categoryCode,
        start: req.body.start,
        end: req.body.end
    };
    model.generateNomorUndian(data, function(result){
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify({ result }));
    });
});

io.on('connection', function(socket){
    console.log('Connected');
    socket.on('disconnect', function(){
        console.log('Disconnect');
    });

    socket.on('result', function(msg){
        switch(msg.categoryCode) {
            case "BSOG":
                io.emit('result-bsog', msg);
                break;
            case "BSOP":
                io.emit('result-bsop', msg);
                break;
            case "ACAC":
                io.emit('result-acac', msg);
                break;
            case "FRDG":
                io.emit('result-frdg', msg);
                break;
        }        
    });

    socket.on('stop', function(msg){
        io.emit('stop', msg);
    });

    socket.on('test', function(msg){
        console.log(msg);
        console.log('Server :' + Date.now());
    });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});