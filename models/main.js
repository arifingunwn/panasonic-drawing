var Connection = require('tedious').Connection;
var config = {
    userName: 'sa',
    password: 'nawadata',
    server: 'localhost',
    options: { database: 'PANASONIC', rowCollectionOnRequestCompletion: true }
};
var Request = require ('tedious').Request;
var TYPES = require('tedious').TYPES;

module.exports.getDrawingParameter = function(category, prizeCode, callback){
    connection = new Connection(config);
    connection.on('connect', function(err){
        if(err) {
            console.log(err);
        }
        var sql = "";
        sql = sql + "SELECT * FROM Category AS c ";
        sql = sql + "INNER JOIN Prize AS p ON c.PkCategoryId = p.FkCategoryId ";
        sql = sql + "WHERE c.CategoryCode = @cat AND p.PrizeCode = @prizeCode";
        var request = new Request(sql, function(err){
            if(err){
                console.log(err);
            }
        });

        request.addParameter('cat', TYPES.VarChar, category);
        request.addParameter('prizeCode', TYPES.VarChar, prizeCode);

        var jsonArray = [];
        request.on('row', function(columns){
            var rowObject = {};
            columns.forEach(function (column) {
                if (column.value === null) {
                    console.log('NULL');
                } else {
                    rowObject[column.metadata.colName] = column.value;
                }
            });

            jsonArray.push(rowObject);
        });

        request.on('doneProc', function(rowCount, more, returnStatus, rows){
            callback(jsonArray);
        });

        connection.execSql(request);
    });
}

module.exports.getDrawingPool = function(category, callback){
    connection = new Connection(config);
    connection.on('connect', function(err){
        if(err) {
            console.log(err);
        }
        var sql = "";
        sql = sql + "SELECT DrawingNumber FROM DrawingPool AS c ";
        sql = sql + "WHERE c.CategoryCode = @cat ORDER BY 1";
        var request = new Request(sql, function(err){
            if(err){
                console.log(err);
            }
        });

        request.addParameter('cat', TYPES.VarChar, category);

        var jsonArray = [];
        request.on('row', function(columns){
            var rowObject = {};
            columns.forEach(function (column) {
                if (column.value === null) {
                    console.log('NULL');
                } else {
                    rowObject[column.metadata.colName] = column.value;
                }
            });

            jsonArray.push(rowObject);
        });

        request.on('doneProc', function(rowCount, more, returnStatus, rows){
            callback(jsonArray);
        });

        connection.execSql(request);
    });
}

module.exports.getWinDrawingNumber = function(category, prizeCode, callback){
    connection = new Connection(config);
    connection.on('connect', function(err){
        if(err) {
            console.log(err);
        }
        var sql = "";
        sql = sql + "SELECT DrawingNumber FROM DrawingResult ";
        sql = sql + "WHERE CategoryCode = @cat AND PrizeCode = @prizeCode";
        var request = new Request(sql, function(err){
            if(err){
                console.log(err);
            }
        });

        request.addParameter('cat', TYPES.VarChar, category);
        request.addParameter('prizeCode', TYPES.VarChar, prizeCode);

        var jsonArray = [];
        request.on('row', function(columns){
            var rowObject = {};
            columns.forEach(function (column) {
                if (column.value === null) {
                    console.log('NULL');
                } else {
                    rowObject[column.metadata.colName] = column.value;
                }
            });

            jsonArray.push(rowObject);
        });

        request.on('doneProc', function(rowCount, more, returnStatus, rows){
            callback(jsonArray);
        });

        connection.execSql(request);
    });
}

module.exports.getDrawingResult = function(category, callback){
    connection = new Connection(config);
    connection.on('connect', function(err){
        if(err) {
            console.log(err);
        }
        var sql = "";
        sql = sql + "SELECT DrawingNumber, CategoryCode, REPLACE(PrizeCode, '.', '') as PrizeCode, WinnerName FROM DrawingResult ";
        sql = sql + "WHERE CategoryCode = @cat";
        var request = new Request(sql, function(err){
            if(err){
                console.log(err);
            }
        });

        request.addParameter('cat', TYPES.VarChar, category);

        var jsonArray = [];
        request.on('row', function(columns){
            var rowObject = {};
            columns.forEach(function (column) {
                if (column.value === null) {
                    console.log('NULL');
                } else {
                    rowObject[column.metadata.colName] = column.value;
                }
            });

            jsonArray.push(rowObject);
        });

        request.on('doneProc', function(rowCount, more, returnStatus, rows){
            callback(jsonArray);
        });

        connection.execSql(request);
    });
};

module.exports.getDrawingResultByPrizeCode = function(categoryCode, prizeCode, callback){
    connection = new Connection(config);
    connection.on('connect', function(err){
        if(err) {
            console.log(err);
        }
        var sql = "";
        sql = sql + "SELECT DR.DrawingNumber, DR.CategoryCode, DR.PrizeCode, DR.WinnerName, C.CategoryName FROM DrawingResult DR ";
        sql = sql + "INNER JOIN Category C ON DR.CategoryCode = C.CategoryCode ";
        sql = sql + "WHERE DR.CategoryCode = @cat AND DR.PrizeCode IN (" + prizeCode.map(d => "'" + d + "'").join(',') + " ) ORDER BY DR.DrawingNumber ";
        console.log(sql);
        var request = new Request(sql, function(err){
            if(err){
                console.log(err);
            }
        });

        request.addParameter('cat', TYPES.VarChar, categoryCode);

        var jsonArray = [];
        request.on('row', function(columns){
            var rowObject = {};
            columns.forEach(function (column) {
                if (column.value === null) {
                    console.log('NULL');
                } else {
                    rowObject[column.metadata.colName] = column.value;
                }
            });

            jsonArray.push(rowObject);
        });

        request.on('doneProc', function(rowCount, more, returnStatus, rows){
            callback(jsonArray);
        });

        connection.execSql(request);
    });
};

module.exports.getPrizeByCategoryCode = function(categoryCode, callback){
    connection = new Connection(config);
    connection.on('connect', function(err){
        if(err) {
            console.log(err);
        }
        var sql = "";
        sql = sql + "SELECT PrizeCode FROM Prize ";
        sql = sql + "WHERE FkCategoryId IN (SELECT PkCategoryId FROM Category WHERE CategoryCode = @cat)";
        console.log(sql);
        var request = new Request(sql, function(err){
            if(err){
                console.log(err);
            }
        });

        request.addParameter('cat', TYPES.VarChar, categoryCode);

        var jsonArray = [];
        request.on('row', function(columns){
            var rowObject = {};
            columns.forEach(function (column) {
                if (column.value === null) {
                    console.log('NULL');
                } else {
                    rowObject[column.metadata.colName] = column.value;
                }
            });

            jsonArray.push(rowObject);
        });

        request.on('doneProc', function(rowCount, more, returnStatus, rows){
            callback(jsonArray);
        });

        connection.execSql(request);
    });
};

module.exports.insertWinNumber = function(data, callback){
    connection = new Connection(config);
    connection.on('connect', function(err){
        if(err) {
            console.log(err);
        }
        var sql = "usp_InsertDrawingResult"
        var request = new Request(sql, function(err){
            if(err){
                console.log(err);
            }
        });

        request.addParameter('DrawingNumber', TYPES.VarChar, data.winNumber);
        request.addParameter('CategoryCode', TYPES.VarChar, data.catCode);
        request.addParameter('PrizeCode', TYPES.VarChar, data.prizeCode);

        request.on('row', function(columns){
        });

        request.on('doneProc', function(rowCount, more, returnStatus, rows){
            callback();
        });

        connection.callProcedure(request);
    });
}

module.exports.getRandomCode = function(categoryCode, callback){
    connection = new Connection(config);
    connection.on('connect', function(err){
        if(err) {
            console.log(err);
        }

        var request = new Request("usp_GetRandomCode", function(err){
            if(err){
                console.log(err);
            }
        });

        request.addParameter('categoryCode', TYPES.VarChar, categoryCode);
        request.addOutputParameter('DrawingNumber', TYPES.VarChar);

        request.on('returnValue', function(parameterName, value, metadata){
            callback({ randomNumber: value });
        });

        connection.callProcedure(request);
    });
}

module.exports.getUnclaimedWinNumber = function(category, prizeCode, callback){
    connection = new Connection(config);
    connection.on('connect', function(err){
        if(err) {
            console.log(err);
        }
        var sql = "";
        sql = sql + "SELECT ROW_NUMBER() OVER (ORDER BY PkDrawingResultId) as [No], * FROM DrawingResult ";
        sql = sql + "WHERE WinnerName = '' ";

        if(category.toUpperCase() != 'ALL'){
            sql = sql + " AND CategoryCode = @cat ";

            if(prizeCode.toUpperCase() != 'ALL'){
                sql = sql + " AND PrizeCode = @prizeCode";
            }
        }

        console.log(sql);
        var request = new Request(sql, function(err){
            if(err){
                console.log(err);
            }
        });
        
        request.addParameter('cat', TYPES.VarChar, category);
        request.addParameter('prizeCode', TYPES.VarChar, prizeCode);

        var jsonArray = [];
        request.on('row', function(columns){
            var rowObject = {};
            columns.forEach(function (column) {
                if (column.value === null) {
                    console.log('NULL');
                } else {
                    rowObject[column.metadata.colName] = column.value;
                }
            });

            jsonArray.push(rowObject);
        });

        request.on('doneProc', function(rowCount, more, returnStatus, rows){
            callback(jsonArray);
        });

        connection.execSql(request);
    });
}

module.exports.getReportWinner = function(callback){
    connection = new Connection(config);
    connection.on('connect', function(err){
        if(err) {
            console.log(err);
        }
        var sql = "";
        sql = sql + "SELECT ROW_NUMBER() OVER (ORDER BY PkDrawingResultId) as [No], * FROM DrawingResult ";

        console.log(sql);
        var request = new Request(sql, function(err){
            if(err){
                console.log(err);
            }
        });

        var jsonArray = [];
        request.on('row', function(columns){
            var rowObject = {};
            columns.forEach(function (column) {
                if (column.value === null) {
                    console.log('NULL');
                } else {
                    rowObject[column.metadata.colName] = column.value;
                }
            });

            jsonArray.push(rowObject);
        });

        request.on('doneProc', function(rowCount, more, returnStatus, rows){
            callback(jsonArray);
        });

        connection.execSql(request);
    });
}

module.exports.generateNomorUndian = function(data, callback){
    connection = new Connection(config);
    connection.on('connect', function(err){
        if(err) {
            console.log(err);
        }
        var sql = "GenerateDrawingPool"
        var request = new Request(sql, function(err){
            if(err){
                console.log(err);
            }
        });

        request.addParameter('categoryCode', TYPES.VarChar, data.categoryCode);
        request.addParameter('start', TYPES.Int, data.start);
        request.addParameter('end', TYPES.Int, data.end);

        request.on('row', function(columns){
        });

        request.on('doneProc', function(rowCount, more, returnStatus, rows){
            console.log(rowCount, more, returnStatus, rows);
            callback('Sukses');
        });

        connection.callProcedure(request);
    });
}

module.exports.voidNomorUndian = function(nomorUndian, callback){
    connection = new Connection(config);
    connection.on('connect', function(err){
        if(err) {
            console.log(err);
        }

        var request = new Request("usp_VoidDrawingNumber", function(err){
            if(err){
                console.log(err);
            }
        });

        request.addParameter('drawingNumber', TYPES.VarChar, nomorUndian);
        request.addOutputParameter('result', TYPES.VarChar);

        request.on('returnValue', function(parameterName, value, metadata){
            callback(value);
        });

        connection.callProcedure(request);
    });
}