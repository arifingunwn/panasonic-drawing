USE [master]
GO
/****** Object:  Database [PANASONIC]    Script Date: 5/9/2019 11:04:47 AM ******/
CREATE DATABASE [PANASONIC]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PANASONIC', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\PANASONIC.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PANASONIC_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\PANASONIC_log.ldf' , SIZE = 139264KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [PANASONIC] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PANASONIC].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PANASONIC] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PANASONIC] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PANASONIC] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PANASONIC] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PANASONIC] SET ARITHABORT OFF 
GO
ALTER DATABASE [PANASONIC] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PANASONIC] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PANASONIC] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PANASONIC] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PANASONIC] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PANASONIC] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PANASONIC] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PANASONIC] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PANASONIC] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PANASONIC] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PANASONIC] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PANASONIC] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PANASONIC] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PANASONIC] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PANASONIC] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PANASONIC] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PANASONIC] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PANASONIC] SET RECOVERY FULL 
GO
ALTER DATABASE [PANASONIC] SET  MULTI_USER 
GO
ALTER DATABASE [PANASONIC] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PANASONIC] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PANASONIC] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PANASONIC] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [PANASONIC] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [PANASONIC] SET QUERY_STORE = OFF
GO
USE [PANASONIC]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [PANASONIC]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 5/9/2019 11:04:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[PkCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](100) NOT NULL,
	[CategoryCode] [varchar](50) NOT NULL,
	[DrawingNumberStart] [int] NOT NULL,
	[DrawingNumberEnd] [int] NOT NULL,
	[FlagDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_PriceCategory] PRIMARY KEY CLUSTERED 
(
	[PkCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DrawingPool]    Script Date: 5/9/2019 11:04:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DrawingPool](
	[PkDrawingPoolId] [bigint] IDENTITY(1,1) NOT NULL,
	[DrawingNumber] [varchar](10) NOT NULL,
	[CategoryCode] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DrawingResult]    Script Date: 5/9/2019 11:04:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DrawingResult](
	[PkDrawingResultId] [int] IDENTITY(1,1) NOT NULL,
	[DrawingNumber] [varchar](10) NOT NULL,
	[CategoryCode] [varchar](10) NOT NULL,
	[PrizeCode] [varchar](50) NOT NULL,
	[WinnerName] [varchar](100) NULL,
	[WinnerPhoneNumber] [varchar](100) NULL,
	[WinnerDealerName] [varchar](100) NULL,
	[WinnerLocation] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_DrawingResult] PRIMARY KEY CLUSTERED 
(
	[PkDrawingResultId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Prize]    Script Date: 5/9/2019 11:04:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Prize](
	[PkPrizeId] [int] IDENTITY(1,1) NOT NULL,
	[FkCategoryId] [int] NOT NULL,
	[PrizeName] [varchar](100) NOT NULL,
	[PrizeCode] [varchar](50) NOT NULL,
	[PrizeQuota] [int] NOT NULL,
	[PrizeRemaining] [int] NOT NULL,
	[FlagDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_PriceSubCategoryId] PRIMARY KEY CLUSTERED 
(
	[PkPrizeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_DrawingResult]    Script Date: 5/9/2019 11:04:48 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_DrawingResult] ON [dbo].[DrawingResult]
(
	[DrawingNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Category] ADD  CONSTRAINT [DF_Category_DrawingNumberStart]  DEFAULT ((0)) FOR [DrawingNumberStart]
GO
ALTER TABLE [dbo].[Category] ADD  CONSTRAINT [DF_Category_DrawingNumberEnd]  DEFAULT ((0)) FOR [DrawingNumberEnd]
GO
ALTER TABLE [dbo].[Category] ADD  CONSTRAINT [DF_PriceCategory_FlagDeleted]  DEFAULT ((0)) FOR [FlagDeleted]
GO
ALTER TABLE [dbo].[DrawingResult] ADD  CONSTRAINT [DF_DrawingResult_CategoryCode]  DEFAULT ('') FOR [CategoryCode]
GO
ALTER TABLE [dbo].[DrawingResult] ADD  CONSTRAINT [DF_DrawingResult_PrizeCode]  DEFAULT ('') FOR [PrizeCode]
GO
ALTER TABLE [dbo].[DrawingResult] ADD  CONSTRAINT [DF_DrawingResult_WinnerName]  DEFAULT ('') FOR [WinnerName]
GO
ALTER TABLE [dbo].[Prize] ADD  CONSTRAINT [DF_Prize_PrizeCode]  DEFAULT ('') FOR [PrizeCode]
GO
ALTER TABLE [dbo].[Prize] ADD  CONSTRAINT [DF_PriceSubCategoryId_FlagDeleted]  DEFAULT ((0)) FOR [FlagDeleted]
GO
/****** Object:  StoredProcedure [dbo].[GenerateDrawingPool]    Script Date: 5/9/2019 11:04:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GenerateDrawingPool] 
	@categoryCode AS VARCHAR(10),
	@start AS INT,
	@end AS INT
AS
BEGIN
	SET NOCOUNT OFF
	DECLARE @i AS INT = @start
	DECLARE @drawingNumber AS VARCHAR(10)
	
	WHILE @i <= @end
	BEGIN
		SELECT @drawingNumber = CONCAT(@categoryCode, RIGHT(CONCAT('0000',@i), 4))
		
		IF NOT EXISTS (SELECT * FROM DrawingPool WHERE DrawingNumber = @drawingNumber)
		BEGIN
			INSERT INTO DrawingPool (DrawingNumber, CategoryCode)
			VALUES (@drawingNumber, @categoryCode) 
		END
		
		SET @i = @i + 1
	END;
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetRandomCode]    Script Date: 5/9/2019 11:04:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetRandomCode] 
	@categoryCode AS VARCHAR(10),
	@DrawingNumber AS VARCHAR(10) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT TOP 1 @DrawingNumber = DrawingNumber FROM DrawingPool WHERE CategoryCode = @categoryCode
	ORDER BY NEWID()
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertDrawingResult]    Script Date: 5/9/2019 11:04:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_InsertDrawingResult] 
	@PrizeCode VARCHAR(50),
	@CategoryCode VARCHAR(20),
	@DrawingNumber VARCHAR(10)
AS
BEGIN
	INSERT INTO DrawingResult (DrawingNumber, CategoryCode, PrizeCode)
	VALUES (@DrawingNumber, @CategoryCode, @PrizeCode)
	
	UPDATE Prize
	SET
		PrizeRemaining = PrizeRemaining - 1
	WHERE PrizeCode = @PrizeCode AND FkCategoryId IN (SELECT PkCategoryId FROM Category WHERE CategoryCode = @CategoryCode)
	
	DELETE FROM DrawingPool WHERE DrawingNumber = @DrawingNumber
END
GO
/****** Object:  StoredProcedure [dbo].[usp_VoidDrawingNumber]    Script Date: 5/9/2019 11:04:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
CREATE PROCEDURE [dbo].[usp_VoidDrawingNumber] 
	@drawingNumber AS VARCHAR(10),
	@result AS VARCHAR(100) OUTPUT
AS
BEGIN
	SET NOCOUNT OFF;
	
	DELETE FROM DrawingPool WHERE DrawingNumber = @drawingNumber
	
	IF @@ROWCOUNT > 0 
		SET @result = CONCAT('Sukses Void ', @drawingNumber) 
	ELSE
		SET @result = 'No Record'
END
GO
USE [master]
GO
ALTER DATABASE [PANASONIC] SET  READ_WRITE 
GO
